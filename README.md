---

## About

Sample code to communicate with Festo Servo Press YJKP using EthernetIP

---


## Pre-requisite

You will need an .eds file for this specific device model.


---

## How to use

1. Paste the .eds file inside C:\TwinCAT\3.1\Config\Io\EtherNetIP\

2. In TwinCAT under Devices, add a new item and choose EtherNet/IP Scanner (EL6652) under Ethernet/IP.

3. Scan for device at the scanner.

TBC....
---

## Help

Contact support@beckhoff.com.sg

---

## Styling syntax

[Markdown cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)

[Markdown preview](https://dillinger.io/)




