FUNCTION DWordFromWord : DWORD
VAR_INPUT
	WordH: WORD;
	WordL: WORD;
END_VAR
VAR
	dwTemp1: DWORD;
	dwTemp2: DWORD;
END_VAR


dwTemp1 := WORD_TO_DWORD(WordH);
dwTemp2 := ROL(dwTemp1,16);
DWordFromWord := dwTemp1 + WORD_TO_DWORD(WordL);
